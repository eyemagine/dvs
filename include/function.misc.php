<?php

/**
 * Returns an array of social media information with their corresponding links
 * 
 * @return array
 */
function socialLinks() {

    return array(
        "facebook"  => "#",
        "twitter"   => "#",
        "instagram" => "#",
        "vimeo"     => "#",
        "youtube"   => "#"
    );
}

function getSection() {

    $uri = explode("/", $_SERVER['REQUEST_URI']);

    return $uri[1];
}