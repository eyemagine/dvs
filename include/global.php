<?php

/**
 * Load configuration file
 */
include 'config.php';

/**
 * Dynamically allocate base path
 */
$uri = reset(explode("?", preg_replace('/^' . preg_quote(ABS_BASE, '/') . '/', '', $_SERVER['REQUEST_URI'])));
define("BASE", str_repeat("../", substr_count($uri, "/")));

/**
 * Load misc functions
 */
include 'function.misc.php';