<div class="products">

    <div class="slide">

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-01.png" width="102" height="106" alt="" />

            <p class="title">Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-02.png" width="102" height="106" alt="" />

            <p class="title">Gray Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-03.png" width="102" height="106" alt="" />

            <p class="title">Blue Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-04.png" width="102" height="106" alt="" />

            <p class="title">Dark Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-05.png" width="102" height="106" alt="" />

            <p class="title">2-Tone Hat</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-06.png" width="102" height="106" alt="" />

            <p class="title">Brown Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-07.png" width="102" height="106" alt="" />

            <p class="title">Green/Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

    </div>

    <div class="slide">

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-01.png" width="102" height="106" alt="" />

            <p class="title">Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-02.png" width="102" height="106" alt="" />

            <p class="title">Gray Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-03.png" width="102" height="106" alt="" />

            <p class="title">Blue Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-04.png" width="102" height="106" alt="" />

            <p class="title">Dark Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-05.png" width="102" height="106" alt="" />

            <p class="title">2-Tone Hat</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-06.png" width="102" height="106" alt="" />

            <p class="title">Brown Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-07.png" width="102" height="106" alt="" />

            <p class="title">Green/Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

    </div>

    <div class="slide">

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-01.png" width="102" height="106" alt="" />

            <p class="title">Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-02.png" width="102" height="106" alt="" />

            <p class="title">Gray Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-03.png" width="102" height="106" alt="" />

            <p class="title">Blue Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-04.png" width="102" height="106" alt="" />

            <p class="title">Dark Shirt</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-05.png" width="102" height="106" alt="" />

            <p class="title">2-Tone Hat</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-06.png" width="102" height="106" alt="" />

            <p class="title">Brown Shoes</p>

            <p class="price">$150.00</p>

        </a>

        <a class="product">

            <img src="<?php echo BASE ?>images/home/product/product-07.png" width="102" height="106" alt="" />

            <p class="title">Green/Gray Shoes</p>

            <p class="price">$150.00</p>

        </a>

    </div>

</div>