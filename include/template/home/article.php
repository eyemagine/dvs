<div class="articles">

    <div class="article overlayWrapper superlink">

        <img src="<?php echo BASE ?>images/home/article/article-01.jpg" width="298" height="377" alt="" class="backgroundImage" />

        <div class="overlay">

            <div class="bg"></div>

            <div class="content">

                <p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi.</p>

                <p><a href="<?php echo BASE ?>#" class="floatRight">Read More &raquo;</a></p>

            </div>

        </div>

    </div>

    <div class="article overlayWrapper superlink">

        <img src="<?php echo BASE ?>images/home/article/article-02.jpg" width="298" height="377" alt="" class="backgroundImage" />

        <div class="overlay">

            <div class="bg"></div>

            <div class="content">

                <p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi.</p>

                <p><a href="<?php echo BASE ?>#" class="floatRight">Read More &raquo;</a></p>

            </div>

        </div>

    </div>

    <div class="article overlayWrapper superlink">

        <img src="<?php echo BASE ?>images/home/article/article-03.jpg" width="298" height="377" alt="" class="backgroundImage" />

        <div class="overlay">

            <div class="bg"></div>

            <div class="content">

                <p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi.</p>

                <p><a href="<?php echo BASE ?>#" class="floatRight">Read More &raquo;</a></p>

            </div>

        </div>

    </div>

    <div class="clear"></div>

</div>