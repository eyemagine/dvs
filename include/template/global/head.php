<link rel="stylesheet" type="text/css" href="<?php echo BASE ?>css/global.css">
<link rel="stylesheet" type="text/css" href="<?php echo BASE ?>css/dropnav.css">

<script type="text/javascript" src="<?php echo BASE ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo BASE ?>js/jquery.dropnav.js"></script>
<script type="text/javascript" src="<?php echo BASE ?>js/global.js"></script>