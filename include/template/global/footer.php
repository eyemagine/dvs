<div id="footer-wrapper">

    <div id="footer">

        <div class="content">

            <div class="section1">

                <ul class="fluidWidth">

                    <li class="fluidItem">Skate</li>
                    <li class="fluidItem">Snow</li>
                    <li class="fluidItem">Moto</li>
                    <li class="fluidStretch"></li>

                </ul>

                <form name="newsletter" action="<?php echo BASE ?>" method="post">

                    <fieldset>

                        <legend>Sign up for Newsletters</legend>

                        <div class="oneline-element">

                            <input type="text" name="newsletterEmail" placeholder="Enter your email address" />

                            <input type="submit" value="Sign Up" />

                        </div>

                    </fieldset>

                </form>

            </div>

            <div class="section2">

                <div class="floatLeft">

                    <h4>Shop</h4>

                    <ul>

                        <li><a href="<?php echo BASE ?>#">Men's</a></li>
                        <li><a href="<?php echo BASE ?>#">Women's</a></li>
                        <li><a href="<?php echo BASE ?>#">Kids</a></li>
                        <li><a href="<?php echo BASE ?>#">Collections</a></li>
                        <li><a href="<?php echo BASE ?>#">Gift Cards</a></li>

                    </ul>

                </div>

                <div class="floatLeft">

                    <h4>Customer Service</h4>

                    <ul>

                        <li><a href="<?php echo BASE ?>#">My Account</a></li>
                        <li><a href="<?php echo BASE ?>#">Order Status</a></li>
                        <li><a href="<?php echo BASE ?>#">Shipping & returns</a></li>
                        <li><a href="<?php echo BASE ?>#">Contact us</a></li>

                    </ul>

                </div>

            </div>

            <div class="section3">

                <?php include 'include/template/global/sitesearch.php' ?>

                <div class="social">

                    <h5>Connect with DVS</h5>

                    <div class="icons">

                        <?php foreach(socialLinks() as $class => $link): ?>

                        <a href="<?php echo $link ?>" class="<?php echo $class == "youtube" ? $class . " alt" : $class ?>"></a>

                        <?php endforeach ?>

                    </div>

                </div>

            </div>

            <div class="section4">

                <img src="<?php echo BASE ?>images/layout/dvs-logo-footer.png" width="73" height="73" alt="DVS Logo" />

                <p>&copy; <?php echo date("Y", time()) ?> DVS Shoes.<br />All rights reserved.</p>

            </div>

            <div class="clear"></div>

        </div>

        <ul class="links fluidWidth">

            <li class="fluidItem"><a href="<?php echo BASE ?>#">About DVS</a></li>
            <li class="fluidItem"><a href="<?php echo BASE ?>#">California Transparency in Supply Chains Act</a></li>
            <li class="fluidItem"><a href="<?php echo BASE ?>#">Privacy Policy</a></li>
            <li class="fluidItem"><a href="<?php echo BASE ?>#">Site Map</a></li>
            <li class="fluidItem"><a href="<?php echo BASE ?>#">Terms of Use</a></li>
            <li class="fluidStretch"></li>

        </ul>

    </div>

</div>