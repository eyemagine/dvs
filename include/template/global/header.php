<div id="header">

    <a href="<?php echo BASE ?>index.html" class="floatLeft logo"><img src="<?php echo BASE ?>images/layout/dvs-header-logo.png" width="199" height="76" alt="DVS Logo" /></a>

    <form name="dealerLocator" action="<?php echo BASE ?>" method="post">

        <fieldset>

            <legend>Dealer Locator</legend>

            <div class="oneline-element">

                <input type="text" name="dealerZip" placeholder="Enter your zip code" />

                <input type="submit" value="Find" />

            </div>

        </fieldset>

    </form>

    <?php include 'include/template/global/sitesearch.php' ?>

    <div class="social floatRight">

        <h5>Connect with DVS</h5>

        <div class="icons">

            <?php foreach(socialLinks() as $class => $link): ?>

            <a href="<?php echo $link ?>" class="<?php echo $class ?>"></a>

            <?php endforeach ?>

        </div>

    </div>

    <div class="clear"></div>

    <div id="nav" class="floatLeft">

        <?php foreach(array('skate', 'snow') as $section): ?>

        <div class="dropNav<?php echo (getSection() == $section ? ' current' : '') . ($section == 'skate' ? ' extraWidth' : '') ?>">

            <div class="trigger"><?php echo ucwords($section) ?></div>

            <ul class="dropdown">

                <li><a href="<?php echo BASE . $section ?>/team.html">Team</a></li>

                <li><a href="<?php echo BASE ?>#">Gallery</a></li>

                <li><a href="<?php echo BASE ?>#">Videos</a></li>

                <li><a href="<?php echo BASE ?>#">Social</a></li>

                <li><a href="<?php echo BASE ?>#">Footwear</a></li>

            </ul>

        </div>

        <?php endforeach ?>

        <a href="<?php echo BASE ?>#">Moto</a>

        <a href="<?php echo BASE ?>#">Shop</a>

    </div>

    <div id="account" class="floatRight">

        <a href="<?php echo BASE ?>#">My Account</a>

        <a href="<?php echo BASE ?>#" class="cart">Cart</a>

    </div>

    <div class="clear"></div>

</div>