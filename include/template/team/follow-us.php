<div class="followUs">

    <h2 class="sectionTitle alignCenter">follow us</h2>

    <div class="clear"></div>

    <?php for ($i = 1; $i < 14; $i++): ?>

    <div class="person">

        <img src="<?php echo BASE ?>images/team/roster/small/team-0<?php echo 0 == ($k = $i % 8) ? 8 : $k ?>.png" width="74" height="74" alt="" />

        <div class="info">

            <h5>James S.</h5>

            <a href="<?php echo BASE ?>#" class="facebook"></a>

            <a href="<?php echo BASE ?>#" class="twitter"></a>

            <div class="clear"></div>

        </div>

    </div>

    <?php endfor ?>

    <div class="clear"></div>

</div>