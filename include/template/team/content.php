<div class="contentWrapper">

    <div class="videos">

        <h2>videos</h2>

        <ul class="content">

            <?php for ($i = 0; $i < 5; $i++): ?>

            <li class="superlink">

                <div class="feature">

                    <img src="<?php echo BASE ?>images/team/event/play-icon.png" width="48" height="47" alt="" class="playIcon" />

                    <img src="<?php echo BASE ?>images/team/event/1.png" width="189" height="155" alt="" />

                </div>

                <div class="text">

                    <h4>Event Teaser</h4>

                    <p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

                    <a href="<?php echo BASE ?>#" class="floatRight">Read More &raquo;</a>

                </div>

                <div class="clear"></div>

            </li>

            <?php endfor ?>

        </ul>

        <a href="<?php echo BASE ?>#">View all Videos ></a>

    </div>

    <div class="blog">

        <h2>The Mountain &amp; Wave Blog</h2>

        <ul class="content">

            <?php for ($i = 0; $i < 5; $i++): ?>

            <li class="superlink">

                <div class="feature">

                    <img src="<?php echo BASE ?>images/team/event/1.png" width="189" height="155" alt="" />

                </div>

                <div class="text">

                    <h4>Event Teaser</h4>

                    <p>Gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>

                    <a href="<?php echo BASE ?>#" class="floatRight">Read More &raquo;</a>

                </div>

                <div class="clear"></div>

            </li>

            <?php endfor ?>

        </ul>

        <a href="<?php echo BASE ?>#">View the Blog ></a>

    </div>

</div>

<div class="clear"></div>