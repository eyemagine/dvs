<div class="roster">

    <div class="slide">

        <?php for ($i = 1; $i < 14; $i++): ?>

        <div class="person overlayWrapper superlink">

            <div class="outline"></div>

            <div class="overlay">

                <div class="bg"></div>

                <div class="content">

                    <a href="<?php echo BASE . getSection() ?>/team/member.html">John Doe</a>

                </div>

            </div>

            <img src="<?php echo BASE ?>images/team/roster/large/team-0<?php echo 0 == ($k = $i % 8) ? 8 : $k ?>.png" width="191" height="191" alt="" />

        </div>

        <?php endfor ?>

    </div>

</div>

<div class="clear"></div>