<?php

/**
 * Set to 0 or comment out to turn off errors
 */
ini_set("display_errors", 1);

/**
 * Defines the root dir of your site
 * 
 * Example values:
 *   /
 *   /shop/
 */
define("ABS_BASE", "/");
