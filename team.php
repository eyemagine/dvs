<?php include 'include/global.php' ?><!DOCTYPE html>
<html>

    <head>

        <title>DVS > Team</title>
        
        <link rel="stylesheet" type="text/css" href="<?php echo BASE ?>css/team.css">

        <?php include 'include/template/global/head.php' ?>

        <script type="text/javascript" src="<?php echo BASE ?>js/jquery.cycle.js"></script>
        <script type="text/javascript" src="<?php echo BASE ?>js/team.js"></script>

    </head>

    <body>

        <?php include 'include/template/global/header.php' ?>

        <!--
            BODY BEGIN
        -->
        <div id="body">

            <?php 

                include 'include/template/team/banner.php';
                include 'include/template/team/roster.php';
                include 'include/template/team/content.php';
                include 'include/template/global/product-recent.php';
                include 'include/template/team/follow-us.php';

            ?>

        </div>
        <!--
            BODY END
        -->

        <?php include 'include/template/global/footer.php' ?>

    </body>

</html>