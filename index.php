<?php include 'include/global.php' ?><!DOCTYPE html>
<html>

    <head>

        <title>DVS</title>
        
        <link rel="stylesheet" type="text/css" href="<?php echo BASE ?>css/home.css">

        <?php include 'include/template/global/head.php' ?>

        <script type="text/javascript" src="<?php echo BASE ?>js/jquery.cycle.js"></script>
        <script type="text/javascript" src="<?php echo BASE ?>js/home.js"></script>

    </head>

    <body>

        <?php include 'include/template/global/header.php' ?>

        <!--
            BODY BEGIN
        -->
        <div id="body">

            <?php include 'include/template/home/banner.php' ?>

            <?php include 'include/template/home/product.php' ?>

            <?php include 'include/template/home/article.php' ?>

        </div>
        <!--
            BODY END
        -->

        <?php include 'include/template/global/footer.php' ?>

    </body>

</html>